const path = require('path');
const fs = require('fs');

module.exports = function() {
    let cache = {};
    let pkgJSON = fs.readFileSync(path.resolve(process.cwd(), './package.json'));
    let content = JSON.parse(pkgJSON);
    let allPlugin = Object.assign(content.devDependencies, content.dependencies);
    for (let key in allPlugin) {
        let pattern = /(^gulp-)|(^slue-)/;
        if (pattern.test(key)) {
            let plugin = require(key);
            key = key.replace(pattern, '').replace(/-(\w)/, function(matchRes, capture) {
                return capture.toUpperCase();
            });
            cache[key] = plugin;
        }
    }
    return cache;
}